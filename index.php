<?php

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger;

require 'vendor/autoload.php';

echo $_SERVER['PHP_SELF'];

d(['books' => [
        ['title' => 'Batman año uno', 'Author' => 'Frank Miller'],
        ['title' => 'Cruzada', 'Author' => 'JEAN DUFAUX'],
        ['title' => 'El Gato Negro', 'Author' => 'Allan Edgar Poe'],
        ['title' => 'DeadPool', 'Author' => 'Rob Riefeld'],
        ['title' => 'Los caminos de los reyes', 'Author' => 'Brandon algo']
    ]
]);

class MessageManager {
    private $logger;

    function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    function sendMessage(string $msg, string $level) {
        if(method_exists($this->logger, $level)) {
            echo "<h3> Existe </h3>";
            $this->logger->$level($msg);
        } else {
            throw new \Exception("Invalid $level Message", 1);
        }
    }
}

class LoggerImpl extends AbstractLogger {
    public function log($level, $message, array $context = array()) {

    }
}

$messageManager = new MessageManager(new LoggerImpl());
$messageManager->sendMessage("Simulacro de mensaje crítico", LogLevel::CRITICAL);



?>
